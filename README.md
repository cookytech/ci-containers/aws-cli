# cookytech/ci-containers/aws-cli
Automatically up-to-date containers for user in CI, making available the aws-cli at all times.


# Image timezone
Some dependencies (less -> tzdata) used to install aws cli need the machine's timezone to be configured.
This image lives in the GMT+5:30 timezone.

For a different timezone, clone this repo, override $IMAGE_TIMEZONE in the gitlab variables alongwith the 
docker credential variables used in the [file](./.gitlab-ci.yml)

# Schedule
This image is updated daily with scheduled pipelines
