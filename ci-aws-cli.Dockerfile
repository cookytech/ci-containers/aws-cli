FROM ubuntu:latest
# Upgrading the distro to latest packages
RUN apt update -y
RUN apt dist-upgrade -y
# Setting timezone to avoid prompt from less installation while setting tzdata (https://dev.to/setevoy/docker-configure-tzdata-and-timezone-during-build-20bk)
ENV TZ=$IMAGE_TIMEZONE
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# Installing dependencies it is
RUN apt install unzip wget less groff -y
RUN wget "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
RUN unzip awscli-exe-linux-x86_64.zip
RUN sh ./aws/install
RUN aws --version
CMD bash
